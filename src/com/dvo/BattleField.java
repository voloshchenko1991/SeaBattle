package com.dvo;


import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class BattleField implements Printable {
    Scanner scanner = new Scanner(System.in);
    Random random = new Random();
    private final boolean[][] field = new boolean[10][10];
    private int iteration;

    BattleField() {
        randomize();
        iteration = 1;
        printArray();
    }

    private void randomize() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = random.nextBoolean();
            }
        }
    }

    private void drawLine() {
        for (int i = 0; i < field.length; i++) {
            System.out.print('-');
        }
        System.out.println();
    }

    private void writeIterationCount() {
        System.out.println();
        System.out.println("Step: " + iteration++);
    }

    public void printArray() {
        drawLine();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j]) {
                    System.out.print("X");
                } else {
                    System.out.print(" ");
                }
            }

            System.out.println();
        }
        writeIterationCount();
        drawLine();
    }

    public void step() throws InvalidCoordinates {
        try {
            System.out.print("Enter X: ");
            int x = scanner.nextInt();
            System.out.print("Enter Y: ");
            int y = scanner.nextInt();

            if (field[x][y]) {
                field[x][y] = false;
                System.out.println("Kill!");
            }
            printArray();
        } catch (InputMismatchException e) {
            scanner.next();
            throw new InputMismatchException();
        }catch (IndexOutOfBoundsException e){
            throw new InvalidCoordinates(e);
        }
    }
}
