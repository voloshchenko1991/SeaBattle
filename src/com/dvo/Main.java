package com.dvo;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        BattleField firstBattleField = new BattleField();
        while(true) {
            try {
                firstBattleField.step();
            } catch (InvalidCoordinates e) {
                System.out.println("Invalid coordinates!");
            } catch (InputMismatchException e) {
                System.out.println("Invalid Input!");
            }
        }
    }
}
